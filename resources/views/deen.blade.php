<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html ng-app="Ad-Deen">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NLASO CRM</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<style type="text/css">
		.title {
			font-size: 20px;
			color: white;
			font-weight: bold;
			height: 50px;
			position: absolute;
			line-height: 50px;
		}
	</style>
	@yield('css')
</head>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">CRM</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">CRM</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
			<img src="{{ asset('img/logo.png') }}" alt="App Logo" style="height: auto; width: 40px; margin: 6px 8px;" />
			<span class="title">জাতীয় আইনগত সহায়তা প্রদান সংস্থা</span>
            <!-- Navbar Right Menu -->
            {{-- @include('comp.notif') --}}
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ asset('img/avatar5.png') }}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ \Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    <a href="{{ url('logout') }}"><i class="fa fa-sign-out"></i> Logout</a>
                </div>
            </div>

            <!-- search form (Optional) -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
                </div>
            </form>
            <!-- /.search form -->

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">Menu</li>
                <!-- Optionally, you can add icons to the links -->
                <li>
                    <a href="{{ url('dashboard') }}">
                        <i class="fa fa-table"></i> <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('call/create') }}">
                        <i class="fa fa-plus"></i> <span>Create Service Seeker Data</span>
                    </a>
                </li>
                @if (\Auth::user()->type == \App\User::$TYPE_SUPER_ADMIN)
                <li>
                    <a href="{{ url('agents') }}">
                        <i class="fa fa-user"></i> <span>Agent Management</span>
                    </a>
                </li>
                @endif
                <li>
                    <a href="{{ url('victim/history') }}">
                        <i class="fa fa-history"></i> <span>Service Seeker History</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('sms') }}">
                        <i class="fa fa-comment"></i> <span>SMS Reports</span>
                    </a>
                </li>
	            <li>
                    <a href="{{ url('facebook') }}">
                        <i class="fa fa-facebook"></i> <span>Facebook Fanpage Reports</span>
                    </a>
                </li>
	            <li>
                    <a href="{{ url('mobile') }}">
                        <i class="fa fa-mobile-phone"></i> <span>App Reports</span>
                    </a>
                </li>
	            <li>
                    <a href="http://bdlaws.minlaw.gov.bd/index.php?page=html&language=english">
                        <i class="fa fa-external-link"></i> <span>Bangladesh Laws</span>
                    </a>
                </li>
                <li>
                    <a href="http://www.nlaso.gov.bd/site/page/d2870052-a5dd-416a-a886-03c0f0622055/%E0%A6%95%E0%A7%87%E0%A6%A8%E0%A7%8D%E0%A6%A6%E0%A7%8D%E0%A6%B0%E0%A7%80%E0%A7%9F-%E0%A6%95%E0%A6%BE%E0%A6%B0%E0%A7%8D%E0%A6%AF%E0%A6%BE%E0%A6%B2%E0%A7%9F" target="_blank">
                        <i class="fa fa-external-link"></i> <span>NLASO Address Book</span>
                    </a>
                </li>
	            <li>
                    <a href="{{ url('doc/outside.pdf') }}">
                        <i class="fa fa-file-text-o"></i> <span>Legal Aid Related Addresses</span>
                    </a>
                </li>
                <li>
                    <a href="http://www.nlaso.gov.bd/site/page/f9ecf3af-a52b-46c1-bc1e-95b2cae88d10/%E0%A6%9C%E0%A7%87%E0%A6%B2%E0%A6%BE-%E0%A6%B2%E0%A6%BF%E0%A6%97%E0%A7%8D%E0%A6%AF%E0%A6%BE%E0%A6%B2-%E0%A6%8F%E0%A6%87%E0%A6%A1-%E0%A6%85%E0%A6%AB%E0%A6%BF%E0%A6%B8" target="_blank">
                        <i class="fa fa-external-link"></i> <span>District Legal Aid Offices</span>
                    </a>
                </li>
            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
	    @yield('body')
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">

        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2016.</strong>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane active" id="control-sidebar-home-tab">
                <h3 class="control-sidebar-heading">Recent Activity</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="#">
                            <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                                <p>Will be 23 on April 24th</p>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

                <h3 class="control-sidebar-heading">Tasks Progress</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="#">
                            <h4 class="control-sidebar-subheading">
                                Custom Template Design
                                <span class="label label-danger pull-right">70%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->
            <!-- Settings tab content -->
            <div class="tab-pane" id="control-sidebar-settings-tab">
                <form method="post">
                    <h3 class="control-sidebar-heading">General Settings</h3>

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Report panel usage
                            <input type="checkbox" class="pull-right" checked>
                        </label>

                        <p>
                            Some information about this general settings option
                        </p>
                    </div>
                    <!-- /.form-group -->
                </form>
            </div>
            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->
@yield('js')

</body>
</html>
